
# Fweb - Formal Web Go Implementation

Fweb is a Go implementation of the
[Formal Web proposals.][proposals]

[proposals]: https://github.com/formal-web/proposals/

## Importing the Package

You can use the `fweb` Go package with the following import path:

```
github.com/formal-web/fweb
```

## Building the `fweb` CLI

All this project's functionality is made available through a 
Command Line Interface (CLI) named `fweb`.

1. [Install the Go programming language.][install]
2. Run `go install github.com/formal-web/fweb/fweb`
to build and install the `fweb` CLI.
3. Run `fweb` to print the help text.

[install]: https://golang.org/doc/install
