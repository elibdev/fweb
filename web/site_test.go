package fweb_test

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/formal-web/fweb"
)

// HTTP client used for testing
type testClient struct {
	*http.Client
	test *testing.T
}

// create a HTTP client that trusts the site's TLS certificate
func newTestClient(test *testing.T, site *fweb.Site) *testClient {
	cert, err := ioutil.ReadFile(site.Cert)
	if err != nil {
		test.Errorf("could not read certificate for site: %s", err)
	}
	certPool := x509.NewCertPool()
	certPool.AppendCertsFromPEM(cert)

	return &testClient{
		&http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs: certPool,
				},
			},
		},
		test,
	}
}

// check that the given value is the expected value
func check(test *testing.T, value, expected interface{}) {
	if value != expected {
		test.Errorf("expected: %v; got: %v", expected, value)
	}
}

// do the request and check its status and if its body contains the string
func checkRequest(client *testClient, request *http.Request, status int, inBody string) {
	method := request.Method
	url := request.URL.String()
	test := client.test
	response, err := client.Do(request)
	if err != nil {
		test.Errorf(`
request attempted: %s %s
got request error: %s
`, method, url, err)
	}
	if status != response.StatusCode {
		test.Errorf(`
 in response to: %s %s
expected status: %d
     got status: %d
`, method, url, status, response.StatusCode)
	}
	body, err := ioutil.ReadAll(response.Body)
	response.Body.Close()
	if err != nil {
		test.Errorf(`
    in response to: %s %s
error reading body: %s
`, method, url, err)
	}
	if !strings.Contains(string(body), inBody) {
		test.Errorf(`
in response to: %s %s
expected body to contain:

%s

actual response body was:

%s
`, method, url, inBody, string(body))
	}
}

func TestLocalConfig(test *testing.T) {
	site := &fweb.Site{}
	site.LocalConfigFallback()
	check(test, site.Address, "localhost:8181")
	check(test, path.Base(site.Dir), "fwebdav")
	check(test, path.Base(site.Cert), "cert.pem")
}

func TestLocalSite(test *testing.T) {
	username, password := "tester", "1234"
	os.Setenv("FWEB_USERNAME", username)
	os.Setenv("FWEB_PASSWORD", password)
	site := &fweb.Site{}
	go site.Start()
	defer site.Stop()
	time.Sleep(time.Millisecond)

	test.Run("RequireWebDAVAuth", func(test *testing.T) {
		client := newTestClient(test, site)
		url := "https://localhost:8181"
		request, _ := http.NewRequest("OPTIONS", url, nil)
		checkRequest(client, request, 401, "")
	})
	test.Run("BasicUsage", func(test *testing.T) {
		// test that webdav server is running
		client := newTestClient(test, site)
		url := "https://localhost:8181"
		request, _ := http.NewRequest("OPTIONS", url, nil)
		request.SetBasicAuth(username, password)
		checkRequest(client, request, 200, "")

		// test that the file server is running
		hiPath := filepath.Join(site.Dir, "hi.txt")
		ioutil.WriteFile(hiPath, []byte("hi!"), os.ModePerm)
		request, _ = http.NewRequest("GET", url+"/hi.txt", nil)
		request.SetBasicAuth(username, password)
		checkRequest(client, request, 200, "hi!")
	})
}
