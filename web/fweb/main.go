/*
Command fweb is a command line interface to all of fweb's functionality.
*/
package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/formal-web/fweb"
)

// Make terminal text output bold using ANSI escape sequences.
func b(text string) string {
	return "\033[1m" + text + "\033[0m"
}

func main() {
	site := fweb.Site{}
	site.LocalConfigFallback()
	isLocal := flag.Bool("local", false, "run locally using all default settings")
	address := flag.String("address", site.Address, "the address to serve the site from")
	dir := flag.String("dir", site.Dir, "the path of the directory to serve")
	cert := flag.String("cert", site.Cert, "the path of the TLS certificate")
	key := flag.String("key", site.Key, "the path of the TLS key")
	flag.Parse()

	// If run with no arguments, just print out help text and exit.
	if len(os.Args) == 1 {
		fmt.Println("\n" + b("fweb") + " runs a Formal Web site.\n")
		flag.Usage()
		fmt.Println("")
		return
	}

	if !*isLocal {
		// override defaults with user-supplied values
		site.Address = *address
		site.Dir = *dir
		site.Cert = *cert
		site.Key = *key
	}

	fmt.Printf("Running a Formal Web site at https://%s ...\n", site.Address)
	site.Start()
}
