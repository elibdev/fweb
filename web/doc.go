/*
Package fweb offers an implementation of the Formal Web proposals.

This package currently implements the following proposals:

	- https://github.com/formal-web/proposals/blob/master/website-server.md

*/
package fweb
