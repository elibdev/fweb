package fweb

import (
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"

	"golang.org/x/net/webdav"
)

/*
Site represents a single website server.

A site expects the environment variables FWEB_USER
and FWEB_PASS to be set to require authentication.
If those environment variables are unset, there will
be no authentication on the server at all.
*/
type Site struct {
	// the domain and port to server the site from
	Address string

	// the path of the directory where the website is stored
	Dir string

	// the path of the TLS certificate to use
	Cert string

	// the path of the TLS key to use
	Key string

	// the underlying http server used to respond to requests
	server *http.Server
}

/*
LocalConfigFallback configures a site to be served locally
using a self-signed TLS certificate if it is not configured.

You can partially configure the Site, and LocalConfigFallback
will configure only the unset fields.
If the address is not set, localhost:8181 will be used.
If the directory is not set, the directory used for the
website is /tmp/fwebdav/ if /tmp/ is the system temp directory.
If the certicate and key are not set, a self-signed pair
will be generated or reused from a past run in /tmp/fwebtls/.

*/
func (site *Site) LocalConfigFallback() {
	if site.Address == "" {
		site.Address = "localhost:8181"
	}

	if site.Dir == "" {
		tmp := os.TempDir()
		// create a fwebdav directory in tmp if it doesn't exist
		tmpDav := filepath.Join(tmp, "fwebdav")
		os.MkdirAll(tmpDav, os.ModePerm)
		site.Dir = tmpDav
	}

	if site.Cert == "" || site.Key == "" {
		site.Cert, site.Key = site.localCert()
	}
}

// wrap an HTTP handler to require basic authentication
func authWrap(handler http.Handler) http.Handler {
	// check for HTTP basic auth
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		authHeader := request.Header.Get("Authorization")
		if authHeader == "" {
			// prompt for authentication
			writer.Header().Set("WWW-Authenticate", `Basic realm="Please sign in."`)
			http.Error(writer, "Unauthorized", http.StatusUnauthorized)
			return
		}
		username := os.Getenv("FWEB_USER")
		password := os.Getenv("FWEB_PASS")
		sentUsername, sentPassword, _ := request.BasicAuth()
		isAuthorized := username == sentUsername && password == sentPassword
		if isAuthorized {
			handler.ServeHTTP(writer, request)
			return
		}
		http.Error(writer, "Forbidden", http.StatusForbidden)
	})
}

/*
Start runs the website, serving it at its address over HTTPS.

An unconfigured Site that is started will configure itself
on startup using LocalConfigFallback.
*/
func (site *Site) Start() {
	site.LocalConfigFallback()
	site.server = &http.Server{
		Addr: site.Address,
		Handler: authWrap(&webdav.Handler{
			FileSystem: webdav.Dir(site.Dir),
			LockSystem: webdav.NewMemLS(),
		}),
	}
	log.Fatal(site.server.ListenAndServeTLS(site.Cert, site.Key))
}

// write a self-signed certificate and key to a temporary directory
// or use existing ones if they are found there
func (site *Site) localCert() (cert string, key string) {
	tmp := os.TempDir()
	// create a fwebtls directory in tmp if it doesn't exist
	fwebTmp := filepath.Join(tmp, "fwebtls")
	os.MkdirAll(fwebTmp, os.ModePerm)
	cert = filepath.Join(fwebTmp, "cert.pem")
	key = filepath.Join(fwebTmp, "key.pem")
	// use existing certificate and key if they exist
	if _, err := os.Stat(cert); err == nil {
		return cert, key
	}
	// write a self-signed TLS certificate and key
	genCert := "/usr/local/go/src/crypto/tls/generate_cert.go"
	cmd := exec.Command("go", "run", genCert, "-host", "localhost")
	// generate the files in our temp directory
	cmd.Dir = fwebTmp
	if err := cmd.Run(); err != nil {
		// return empty strings to signify no certificate
		return "", ""
	}
	return cert, key
}

/*
Stop stops a running website server.

This method does not allow all current connections to finish
before it returns and may cause client connection errors.
*/
func (site *Site) Stop() {
	err := site.server.Close()
	if err != nil {
		log.Fatal(err)
	}
}
